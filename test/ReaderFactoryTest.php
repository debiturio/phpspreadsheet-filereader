<?php

namespace Debiturio\PHPSpreadsheetFilereaderTest;

use Debiturio\PHPSpreadsheetFilereader\Reader;
use Debiturio\PHPSpreadsheetFilereader\ReaderFactory;
use PHPUnit\Framework\TestCase;

class ReaderFactoryTest extends TestCase
{

    /**
     * @dataProvider dataProvider
     * @param $path
     */
    public function testGetReader($path)
    {
        $factory = new ReaderFactory();
        $this->assertInstanceOf(Reader::class, $factory->getReader($path));
    }

    public function dataProvider(): array
    {
        return [
            [
                __DIR__ . '/data/dogs.csv'
            ],
            [
                __DIR__ . '/data/dogs.xlsx'
            ]
        ];
    }
}
