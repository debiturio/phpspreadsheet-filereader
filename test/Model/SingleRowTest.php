<?php

namespace Debiturio\PHPSpreadsheetFilereaderTest\Model;

use Debiturio\PHPSpreadsheetFilereader\Model\SingleRow;
use Debiturio\SpreadsheetToRestCore\FileReader\CellIteratorInterface;
use PHPUnit\Framework\TestCase;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class SingleRowTest extends TestCase
{

    public function testGetCells()
    {
        $worksheet = $this->createMock(Worksheet::class);
        $row = new SingleRow(10, $worksheet);

        $this->assertInstanceOf(CellIteratorInterface::class, $row->getCells());
    }

    public function testGetCellByColumnIndex()
    {
        $cell = $this->createMock(\PhpOffice\PhpSpreadsheet\Cell\Cell::class);
        $cell->method('getValue')->willReturn('foo');
        $cell->method('getColumn')->willReturn('E');
        $worksheet = $this->createMock(Worksheet::class);
        $worksheet->expects($this->once())->method('getCellByColumnAndRow')->with(5, 10)->willReturn($cell);

        $row = new SingleRow(10, $worksheet);

        $result = $row->getCellByColumnIndex(5);
        $this->assertEquals('foo', $result->getValue());

    }
}
