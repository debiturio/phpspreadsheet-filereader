<?php

namespace Debiturio\PHPSpreadsheetFilereaderTest\Model;

use Debiturio\PHPSpreadsheetFilereader\Model\CellCollection;
use Debiturio\PHPSpreadsheetFilereader\Model\RowCollection;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PHPUnit\Framework\TestCase;

class RowCollectionTest extends TestCase
{
    public function testIterator()
    {
        $worksheet = $this->createMock(Worksheet::class);
        $worksheet->expects($this->exactly(3))->method('getHighestDataRow')
            ->willReturnOnConsecutiveCalls(true, true, false);


        $iterator = new RowCollection($worksheet);

        $i = 0;

        foreach ($iterator as $row) {
            $i++;
            $this->assertEquals($i, $row->getIndex());
            $this->assertInstanceOf(CellCollection::class, $row->getCells());
        }

        $this->assertEquals(2, $i);

    }
}
