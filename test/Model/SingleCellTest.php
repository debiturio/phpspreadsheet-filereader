<?php

namespace Debiturio\PHPSpreadsheetFilereaderTest\Model;

use Debiturio\PHPSpreadsheetFilereader\Model\SingleCell;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PHPUnit\Framework\TestCase;

class SingleCellTest extends TestCase
{

    public function test__construct()
    {
        $cell = new SingleCell($colIndex = 5, $value = 'foo');
        $this->assertEquals($colIndex, $cell->getColumnIndex());
        $this->assertEquals($value, $cell->getValue());
    }

    public function testFromPHPSpreadsheetCell()
    {
        $phpSpreadsheetCell = $this->createMock(Cell::class);
        $phpSpreadsheetCell->expects($this->once())->method('getColumn')->willReturn('AB');
        $phpSpreadsheetCell->expects($this->once())->method('getValue')->willReturn('test');

        $cell = SingleCell::fromPHPSpreadsheetCell($phpSpreadsheetCell);
        $this->assertEquals(28, $cell->getColumnIndex());
        $this->assertEquals('test', $cell->getValue());
    }
}
