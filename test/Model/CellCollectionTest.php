<?php

namespace Debiturio\PHPSpreadsheetFilereaderTest\Model;

use Debiturio\PHPSpreadsheetFilereader\Model\CellCollection;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PHPUnit\Framework\TestCase;

class CellCollectionTest extends TestCase
{

    public function testIterator()
    {
        $rowIndex = 1;
        $worksheet = $this->createMock(Worksheet::class);

        $worksheet->expects($this->exactly(4))
            ->method('cellExistsByColumnAndRow')
            ->withConsecutive(
                [1, $rowIndex],
                [2, $rowIndex],
                [3, $rowIndex]
            )
            ->willReturnOnConsecutiveCalls(true, true, true, false);

        $cellOne = $this->createMock(Cell::class);
        $cellOne->expects($this->once())->method('getColumn')->willReturn('A');
        $cellOne->expects($this->once())->method('getValue')->willReturn($valueOne = random_int(1,99));

        $cellTwo = $this->createMock(Cell::class);
        $cellTwo->expects($this->once())->method('getColumn')->willReturn('A');
        $cellTwo->expects($this->once())->method('getValue')->willReturn($valueTwo = random_int(1,99));

        $cellThree = $this->createMock(Cell::class);
        $cellThree->expects($this->once())->method('getColumn')->willReturn('A');
        $cellThree->expects($this->once())->method('getValue')->willReturn($valueThree = random_int(1,99));

        $worksheet->expects($this->atLeastOnce())
            ->method('getCellByColumnAndRow')
            ->withConsecutive(
                [1, $rowIndex],
                [2, $rowIndex],
                [3, $rowIndex]
            )->willReturnOnConsecutiveCalls(
                $cellOne,
                $cellTwo,
                $cellThree
            );

        $iterator = new CellCollection($worksheet, $rowIndex);

        $cmp = [];

        foreach ($iterator as $cell) {
            $cmp[] = $cell->getValue();
        }

        $this->assertEquals(
            [$valueOne, $valueTwo, $valueThree],
            $cmp
        );

    }
}
