<?php

namespace Debiturio\PHPSpreadsheetFilereaderTest;

use Debiturio\PHPSpreadsheetFilereader\ChunkReadFilter;
use PHPUnit\Framework\TestCase;

class ChunkReadFilterTest extends TestCase
{

    private ChunkReadFilter $filter;

    protected function setUp(): void
    {
        $this->filter = new ChunkReadFilter();
    }

    /**
     * @dataProvider dataProvider
     */
    public function testReadCell(int $offset, int $chunkSize, int $requestedRow, bool $expectedResult = true)
    {
        $this->filter->setOffset($offset);
        $this->filter->setChunk($chunkSize);
        $this->assertEquals($expectedResult, $this->filter->readCell(1, $requestedRow));
    }

    public function dataProvider(): array
    {
        return [
            [
                0,
                10,
                0
            ],
            [
                10,
                20,
                15
            ],
            [
                10,
                20,
                20
            ],
            [
                0,
                10,
                11,
                false
            ],
            [
                5,
                10,
                0,
                false
            ]
        ];
    }
}
