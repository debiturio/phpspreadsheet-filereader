<?php

namespace Debiturio\PHPSpreadsheetFilereaderTest;

use Debiturio\PHPSpreadsheetFilereader\Reader;
use Debiturio\SpreadsheetToRestCore\FileReader\RowIterator;
use PHPUnit\Framework\TestCase;

class ReaderTest extends TestCase
{

    /**
     * @dataProvider dataTotalRowsProvider
     * @param string $filePath
     * @param int $numberOfRows
     */
    public function testGetTotalRows(string $filePath, int $numberOfRows)
    {
        $reader = new Reader($filePath);
        $this->assertEquals($numberOfRows, $reader->getTotalRows());
    }

    public function dataTotalRowsProvider(): array
    {
        return [
            [
                __DIR__ . '/data/dogs.csv',
                371
            ],
            [
                __DIR__ . '/data/dogs.xlsx',
                371
            ]
        ];
    }

    /**
     * @dataProvider dataRowProvider
     * @param string $filePath
     * @param int $offset
     * @param array $expectedDogsData
     * @param int $startNumber
     */
    public function testGetRows(string $filePath, int $offset, int $numberOfRows, array $expectedDogsData, int $startNumber)
    {
        $reader = new Reader($filePath);

        $rows = $reader->getRows($offset, $numberOfRows);

        $cmp = [];

        foreach ($rows as $row) {
            foreach ($row->getCells() as $cell) {

                switch ($cell->getColumnIndex()) {
                    case 1:
                        break;
                    case 2:
                        $cmp[] = $cell->getValue();
                        break;
                }
            }
        }

        $this->assertEquals($expectedDogsData, $cmp);

    }

    public function dataRowProvider(): array
    {
        return [
            [
                __DIR__ . '/data/dogs.csv',
                1,
                15,
                [
                    'Art',
                    'English Pointer',
                    'English Setter',
                    'Kerry Blue Terrier',
                    'Cairn Terrier',
                    'English Cocker Spaniel',
                    'Gordon Setter',
                    'Airedale Terrier',
                    'Australian Terrier',
                    'Bedlington Terrier',
                    'Border Terrier',
                    'Bullterrier',
                    'Foxterrier Glatthaar',
                    'English Toy Terrier',
                    'Västgötaspets'
                ],
                1
            ],
            [
                __DIR__ . '/data/dogs.xlsx',
                1,
                15,
                [
                    'Art',
                    'English Pointer',
                    'English Setter',
                    'Kerry Blue Terrier',
                    'Cairn Terrier',
                    'English Cocker Spaniel',
                    'Gordon Setter',
                    'Airedale Terrier',
                    'Australian Terrier',
                    'Bedlington Terrier',
                    'Border Terrier',
                    'Bullterrier',
                    'Foxterrier Glatthaar',
                    'English Toy Terrier',
                    'Västgötaspets'
                ],
                1
            ]
        ];
    }
}
