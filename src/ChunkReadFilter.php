<?php
declare(strict_types=1);

namespace Debiturio\PHPSpreadsheetFilereader;


use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;

class ChunkReadFilter implements IReadFilter
{
    private int $chunk = 100;
    private int $offset = 0;

    /**
     * @param int $chunk
     */
    public function setChunk(int $chunk): void
    {
        $this->chunk = $chunk;
    }

    /**
     * @param int $offset
     */
    public function setOffset(int $offset): void
    {
        $this->offset = $offset;
    }

    public function readCell($column, $row, $worksheetName = '')
    {
        $start = $this->offset;
        $end = $this->offset + $this->chunk;

        if (($row === 1) || ($row >= $start && $row < $end)) return true;

        return false;
    }
}