<?php
declare(strict_types=1);

namespace Debiturio\PHPSpreadsheetFilereader;


use Debiturio\SpreadsheetToRestCore\FileReader\SpreadsheetFileReaderFactoryInterface;
use Debiturio\SpreadsheetToRestCore\FileReader\SpreadsheetFileReaderInterface;

class ReaderFactory implements SpreadsheetFileReaderFactoryInterface
{
    public function getReader(string $filePath): SpreadsheetFileReaderInterface
    {
        return new Reader($filePath);
    }
}