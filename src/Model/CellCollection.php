<?php
declare(strict_types=1);

namespace Debiturio\PHPSpreadsheetFilereader\Model;


use Debiturio\SpreadsheetToRestCore\FileReader\CellInterface;
use Debiturio\SpreadsheetToRestCore\FileReader\CellIteratorInterface;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class CellCollection implements CellIteratorInterface
{
    private int $currentColumnIndex = 1;

    public function __construct(private Worksheet $worksheet, private int $rowIndex)
    {
    }

    public function rewind(): void
    {
        $this->currentColumnIndex = 1;
    }

    public function next()
    {
        $this->currentColumnIndex++;
    }

    public function valid(): bool
    {
        return $this->worksheet->cellExistsByColumnAndRow($this->currentColumnIndex, $this->rowIndex);
    }

    public function key(): int
    {
        return $this->currentColumnIndex;
    }

    public function current(): CellInterface
    {
        $cell = $this->worksheet->getCellByColumnAndRow($this->currentColumnIndex, $this->rowIndex);
        return SingleCell::fromPHPSpreadsheetCell($cell);
    }
}