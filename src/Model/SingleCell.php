<?php
declare(strict_types=1);

namespace Debiturio\PHPSpreadsheetFilereader\Model;


use Debiturio\SpreadsheetToRestCore\FileReader\CellInterface;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;

class SingleCell implements CellInterface
{
    public function __construct(private int $columnIndex, private mixed $value)
    {
    }

    public static function fromPHPSpreadsheetCell(\PhpOffice\PhpSpreadsheet\Cell\Cell $cell): self
    {
        return new self(Coordinate::columnIndexFromString($cell->getColumn()), $cell->getValue());
    }

    /**
     * @return int
     */
    public function getColumnIndex(): int
    {
        return $this->columnIndex;
    }

    /**
     * @return mixed
     */
    public function getValue(): mixed
    {
        return $this->value;
    }
}