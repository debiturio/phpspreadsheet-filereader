<?php
declare(strict_types=1);

namespace Debiturio\PHPSpreadsheetFilereader\Model;


use Debiturio\SpreadsheetToRestCore\FileReader\RowInterface;
use Debiturio\SpreadsheetToRestCore\FileReader\RowIteratorInterface;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class RowCollection implements RowIteratorInterface
{

    private int $currentRowIndex = 1;

    public function __construct(private Worksheet $worksheet, private int $offset = 1)
    {
        $this->currentRowIndex = $this->offset;
    }

    public function rewind()
    {
        $this->currentRowIndex = $this->offset;
    }

    public function next()
    {
        $this->currentRowIndex++;
    }

    public function key(): int
    {
        return $this->currentRowIndex;
    }

    public function valid(): bool
    {
        return $this->currentRowIndex <= $this->worksheet->getHighestDataRow();
    }

    public function current(): RowInterface
    {
        return new SingleRow($this->currentRowIndex, $this->worksheet);
    }
}