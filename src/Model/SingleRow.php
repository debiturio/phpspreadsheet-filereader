<?php
declare(strict_types=1);

namespace Debiturio\PHPSpreadsheetFilereader\Model;


use Debiturio\SpreadsheetToRestCore\FileReader\CellInterface;
use Debiturio\SpreadsheetToRestCore\FileReader\CellIteratorInterface;
use Debiturio\SpreadsheetToRestCore\FileReader\RowInterface;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class SingleRow implements RowInterface
{
    public function __construct(private int $index, private Worksheet $worksheet)
    {
    }

    /**
     * @return int
     */
    public function getIndex(): int
    {
        return $this->index;
    }

    /**
     * @return CellIteratorInterface
     */
    public function getCells(): CellIteratorInterface
    {
        return new CellCollection($this->worksheet, $this->index);
    }

    public function getCellByColumnIndex(int $columnIndex): ?CellInterface
    {
        $cell = $this->worksheet->getCellByColumnAndRow($columnIndex, $this->index);
        return SingleCell::fromPHPSpreadsheetCell($cell);
    }
}