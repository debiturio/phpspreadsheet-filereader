<?php
declare(strict_types=1);

namespace Debiturio\PHPSpreadsheetFilereader;


use Debiturio\PHPSpreadsheetFilereader\Model\RowCollection;
use Debiturio\SpreadsheetToRestCore\FileReader\RowIteratorInterface;
use Debiturio\SpreadsheetToRestCore\FileReader\SpreadsheetFileReaderInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\IReader;

class Reader implements SpreadsheetFileReaderInterface
{
    private ChunkReadFilter $chunkFilter;
    private IReader $reader;

    public function __construct(private string $filePath)
    {
        $this->chunkFilter = new ChunkReadFilter();

        $this->reader = self::getInitReader($this->filePath);
        $this->reader->setReadFilter($this->chunkFilter);

        if ($this->reader instanceof Csv) {
            $this->reader->setInputEncoding(\PhpOffice\PhpSpreadsheet\Reader\Csv::GUESS_ENCODING);
            $this->reader->setFallbackEncoding('ISO-8859-2');
        }
    }

    private static function getInitReader(string $filePath): IReader
    {
        $reader = IOFactory::createReaderForFile($filePath);
        $reader->setReadDataOnly(true);
        return $reader;
    }

    public function getTotalRows(): int
    {
        $reader = self::getInitReader($this->filePath);
        $spreadsheet = $reader->load($this->filePath);
        $worksheet = $spreadsheet->getActiveSheet();
        return $worksheet->getHighestDataRow();
    }

    public function getRows(int $offset = 0, int $rows = null): RowIteratorInterface
    {

        $this->chunkFilter->setOffset($offset);
        $this->chunkFilter->setChunk($rows);

        $spreadsheet = $this->reader->load($this->filePath);
        $worksheet = $spreadsheet->getActiveSheet();

        return new RowCollection($worksheet, $offset > 1 ? $offset : 1);
    }
}